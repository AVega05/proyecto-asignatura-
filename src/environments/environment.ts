// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};

export const firebaseConfig = {
  apiKey: "AIzaSyDOdmVBlZIN5mQoMK8BA0eb2U32-BCELng",
  authDomain: "viviendapp-3549e.firebaseapp.com",
  databaseURL: "https://viviendapp-3549e.firebaseio.com",
  projectId: "viviendapp-3549e",
  storageBucket: "viviendapp-3549e.appspot.com",
  messagingSenderId: "311555695071",
  appId: "1:311555695071:web:55f8dc941b19bae0"
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
