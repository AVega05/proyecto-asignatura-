import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument, DocumentReference } from '@angular/fire/firestore';
import { map, take } from 'rxjs/operators';
import { Observable } from 'rxjs';

export interface Vivienda {
  id?: string,
  Cod: string,
  Cimiento: Number,
  Estructura: Number,
  Electricidad: Number,
  Muro: Number,
  Revestimiento: Number,
  Separacion: Number,
  Manzana_id: string
}

@Injectable({
  providedIn: 'root'
})
export class ViviendasService {

  private viviendaCollection: AngularFirestoreCollection<Vivienda>;
 
  constructor(private afs: AngularFirestore) {
    this.viviendaCollection = this.afs.collection<Vivienda>('Viviendas');
  }
 
  getAll(): Observable<Vivienda[]> {
    return this.viviendaCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }
 
  get(id: string): Observable<Vivienda> {
    return this.viviendaCollection.doc<Vivienda>(id).valueChanges().pipe(
      take(1),
      map(vivienda => {
        vivienda.id = id;
        return vivienda
      })
    );
  }

  getForManzana(idManzana: string): Observable<any> {
    return this.afs.collection('Viviendas', ref=> ref.where('Manzana_id', '==', idManzana))
    .snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }
 
  add(vivienda: Vivienda): Promise<DocumentReference> {
    return this.viviendaCollection.add(vivienda);
  }

  delete(id: string): Promise<void> {
    return this.viviendaCollection.doc(id).delete();
  }

  update(vivienda: Vivienda): any {
    var viviendaRef = this.viviendaCollection.doc(vivienda.id);
    return viviendaRef.update({
      Cimiento: vivienda.Cimiento,
      Estructura: vivienda.Estructura,
      Electricidad: vivienda.Electricidad,
      Muro: vivienda.Muro,
      Revestimiento: vivienda.Revestimiento,
      Separacion: vivienda.Separacion,
    });
  }
}
