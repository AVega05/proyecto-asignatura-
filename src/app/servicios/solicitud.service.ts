import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument, DocumentReference } from '@angular/fire/firestore';
import { map, take } from 'rxjs/operators';
import { Observable } from 'rxjs';

export interface Solicitud {
  id?: string,
  Estado: number,
  Fecha: string,
  Kit: string,
  Manzana: string,
  Vivienda: string,
  Vivienda_id: string,
  usuario: string
}

@Injectable({
  providedIn: 'root'
})
export class SolicitudService {

  private solicitudColleciton: AngularFirestoreCollection<Solicitud>;
 
  constructor(private afs: AngularFirestore) {
    this.solicitudColleciton = this.afs.collection<Solicitud>('Solicitudes');
  }
 
  getAll(): Observable<Solicitud[]> {
    return this.solicitudColleciton.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }
 
  get(id: string): Observable<Solicitud> {
    return this.solicitudColleciton.doc<Solicitud>(id).valueChanges().pipe(
      take(1),
      map(solicitud => {
        solicitud.id = id;
        return solicitud
      })
    );
  }

  getByViviendaIdAndKit(viviendaId: string, kit: string): Observable<any> {
    return this.afs.collection('Solicitudes', ref=> ref.where('Vivienda_id', '==', viviendaId).where('Kit', '==', kit))
    .snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }
 
  add(solicitud: Solicitud): Promise<DocumentReference> {
    return this.solicitudColleciton.add(solicitud);
  }

  delete(id: string): Promise<void> {
    return this.solicitudColleciton.doc(id).delete();
  }

  update(solicitud: Solicitud): any {
    var solicitudRef = this.solicitudColleciton.ref.doc(solicitud.id);
    return solicitudRef.update({
      Estado: solicitud.Estado,
      Fecha: solicitud.Fecha,
      Kit: solicitud.Kit,
      Manzana: solicitud.Manzana,
      Vivienda: solicitud.Vivienda
    });
  }
}
