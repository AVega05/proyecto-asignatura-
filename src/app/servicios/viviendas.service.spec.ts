import { TestBed } from '@angular/core/testing';

import { ViviendasService } from './viviendas.service';

describe('ViviendasService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ViviendasService = TestBed.get(ViviendasService);
    expect(service).toBeTruthy();
  });
});
