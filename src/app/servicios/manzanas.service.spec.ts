import { TestBed } from '@angular/core/testing';

import { ManzanasService } from './manzanas.service';

describe('ManzanasService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ManzanasService = TestBed.get(ManzanasService);
    expect(service).toBeTruthy();
  });
});
