import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
 

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private aFireAuth : AngularFireAuth, private alertController : AlertController, private router : Router) {  }

  login(email:string, password:string){

    this.aFireAuth.auth.signInWithEmailAndPassword(email, password).then(res => {
      localStorage.setItem("user", JSON.stringify(res));
      this.router.navigateByUrl("/vivienda");
    }).catch(err => {
      this.errorAlert();

      localStorage.clear()
    })
  }

  async errorAlert() {
    const alert = await this.alertController.create({
      header: 'ERROR',
      message: 'Ocurrió un problema al intentar entrar.',
      buttons: ['OK']
    });

    await alert.present();
  }
}
