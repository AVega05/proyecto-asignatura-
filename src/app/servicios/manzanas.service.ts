import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, DocumentReference } from '@angular/fire/firestore';
import { map, take } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Vivienda } from './viviendas.service';

export interface Manzana {
  id: string,
  Nombre: string,
  Viviendas: Vivienda[]
}

@Injectable({
  providedIn: 'root'
})
export class ManzanasService {

  private manzanaCollection: AngularFirestoreCollection<Manzana>;
 
  constructor(private afs: AngularFirestore) {
    this.manzanaCollection = this.afs.collection<Manzana>('Manzanas');
    
  }
 
  getAll(): Observable<Manzana[]> {
    return this.manzanaCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }
 
  get(id: string): Observable<Manzana> {
    return this.manzanaCollection.doc<Manzana>(id).valueChanges().pipe(
      take(1),
      map(manzana => {
        manzana.id = id;
        return manzana
      })
    );
  }
 
  add(idea: Manzana): Promise<DocumentReference> {
    return this.manzanaCollection.add(idea);
  }
 
  delete(id: string): Promise<void> {
    return this.manzanaCollection.doc(id).delete();
  }
}
