import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', loadChildren: './componentes/login/login.module#LoginPageModule' },
  { path: 'vivienda', loadChildren: './componentes/vivienda/vivienda.module#ViviendaPageModule' },
  { path: 'solicitudes', loadChildren: './componentes/solicitud/solicitud.module#SolicitudPageModule' },
  { path: 'estado', loadChildren: './componentes/estado/estado.module#EstadoPageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
