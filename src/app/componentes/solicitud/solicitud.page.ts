import { Component, OnInit } from '@angular/core';
import { Solicitud, SolicitudService } from 'src/app/servicios/solicitud.service';
import { Observable } from 'rxjs';
import { AlertController } from '@ionic/angular';
import { ViviendasService } from 'src/app/servicios/viviendas.service';
import { Router } from '@angular/router';
import * as moment from 'moment';


@Component({
  selector: 'app-solicitud',
  templateUrl: './solicitud.page.html',
  styleUrls: ['./solicitud.page.scss'],
})
export class SolicitudPage implements OnInit {
  
  date: string
  solicitudes: any[]

  constructor(private solicitudService: SolicitudService, private viviendasService: ViviendasService, private alertController: AlertController, private router: Router) {
    var currentUser = localStorage.getItem("user");
    if (currentUser == null) this.router.navigateByUrl("/login");

    if (!JSON.parse(currentUser).user.email.includes("bodeguero")) {
      this.router.navigateByUrl("/vivienda");
    }
  }

  async ngOnInit() {
    let solicitudes = await this.solicitudService.getAll();
    solicitudes.subscribe(solicitudes => {
      this.solicitudes = solicitudes;
    });
  }

  procesarSolicitud(solicitud: Solicitud) {
    solicitud.Estado = 2;
    solicitud.Fecha = moment (new Date().toString()).format('MM/DD/YYYY HH:mm');
    this.solicitudService.update(solicitud);

    this.viviendasService.get(solicitud.Vivienda_id).subscribe(vivienda => {
      switch (solicitud.Kit) {
        case 'Estructura':
          vivienda.Estructura = 2;
          break;
        case 'Cimiento':
          vivienda.Cimiento = 2;
          break;
        case 'Revestimiento':
          vivienda.Revestimiento = 2;
          break;
        case 'Electricidad':
          vivienda.Electricidad = 2;
          break;
        case 'Separacion':
          vivienda.Separacion = 2;
          break;
        case 'Muro':
          vivienda.Muro = 2;
          break;
      }

      this.viviendasService.update(vivienda);
      this.successAlert();
    });
  }

  async successAlert() {
    const alert = await this.alertController.create({
      header: 'LISTO',
      message: 'Estado de solicitud cambiado a PROCESADO',
      buttons: ['OK']
    });

    await alert.present();
  }

  logout() {
    localStorage.clear();
    this.router.navigateByUrl("/login");
  }

}
