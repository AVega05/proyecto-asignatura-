import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../servicios/auth.service"
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  email: string;
  password: string;

  constructor(private authService : AuthService, private router : Router) { }

  ngOnInit() {
    if(localStorage.getItem("user") != null){
      this.router.navigateByUrl("/vivienda");
    }
  }

  onSubmitLogin(){
    this.authService.login(this.email, this.password)
  }

}
