import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { ViviendasService, Vivienda } from '../../servicios/viviendas.service';
import { ManzanasService, Manzana } from './../../servicios/manzanas.service';
import { SolicitudService, Solicitud } from './../../servicios/solicitud.service';
import { DetalleSolicitud } from '../detalleSolicitud/detalleSolicitud.page';
import * as moment from 'moment';


@Component({
  selector: 'app-vivienda',
  templateUrl: './vivienda.page.html',
  styleUrls: ['./vivienda.page.scss'],
})
export class ViviendaPage implements OnInit {

  viviendas: Observable<Vivienda[]>
  manzanas: Manzana[]
  date: string


  constructor(
    private router: Router,
    public alertController: AlertController,
    private viviendasService: ViviendasService,
    private manzanasService: ManzanasService,
    private solicitudesService: SolicitudService,
    public modalController: ModalController) {
  }

  ngOnInit() {
    this.setManzanas();

    var currentUser = localStorage.getItem("user");
    if (currentUser == null) this.router.navigateByUrl("/login");
    if (!JSON.parse(currentUser).user.email.includes("trabajador")) {
      this.router.navigateByUrl("/solicitudes");
    }
  }

  async setManzanas() {
    this.manzanas = [];
    let manzanas = await this.manzanasService.getAll();
    await manzanas.subscribe(manzanas => {
      manzanas.forEach(manzana => {
        this.viviendasService.getForManzana(manzana.id).subscribe((res: Vivienda[]) => {
          manzana.Viviendas = res;
          this.manzanas.push(manzana);
        });
      });
    });
  }

  solicitud(manzana: Manzana, vivienda: Vivienda, tipo: string, estado: Number) {
    if (estado == 0) {
      this.confirmarSolicitud(manzana, vivienda, tipo, estado);
    }
    if (estado != 0) {
      this.detalleSolicitudModal(vivienda, tipo);
    }
  }

  async confirmarSolicitud(manzana: Manzana, vivienda: Vivienda, tipo: string, estado: Number) {
    const alert = await this.alertController.create({
      header: 'Confirmar Solicitud!',
      message: 'Desea confirmar la peticion del kit <strong>' + tipo + '</strong>',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
        }, {
          text: 'Confirmar',
          handler: () => {
            switch (tipo) {
              case 'Estructura':
                vivienda.Estructura = 1;
                break;
              case 'Cimiento':
                vivienda.Cimiento = 1;
                break;
              case 'Revestimiento':
                vivienda.Revestimiento = 1;
                break;
              case 'Electricidad':
                vivienda.Electricidad = 1;
                break;
              case 'Separacion':
                vivienda.Separacion = 1;
                break;
              case 'Muro':
                vivienda.Muro = 1;
                break;
            }
            let solicitud: Solicitud = {
              Estado: 1,
              Kit: tipo,
              Fecha: moment (new Date().toString()).format('MM/DD/YYYY HH:mm'),
              Manzana: manzana.Nombre,
              Vivienda: vivienda.Cod,
              Vivienda_id: vivienda.id,
              usuario: JSON.parse(localStorage.getItem("user")).user.email
            };
            this.solicitudesService.add(solicitud);
            this.viviendasService.update(vivienda);
            alert.dismiss();
          }
        }
      ]
    });

    await alert.present();
  }

  async detalleSolicitudModal(vivienda: Vivienda, tipo: string) {
    var solicitud = this.solicitudesService.getByViviendaIdAndKit(vivienda.id, tipo);
    solicitud.subscribe(async solicitud => {
      const modal = await this.modalController.create({
        component: DetalleSolicitud,
        componentProps: {
          'solicitud': solicitud[0],
          'vivienda': vivienda,
        }
      });
      return await modal.present();
    });
  }

  logout() {
    localStorage.clear();
    this.router.navigateByUrl("/login");
  }

  /**
   * Metodo crea una vivienda
   */
  // createTest(){
  //   let vivienda : Vivienda = {
  //     id: "",
  //     Cimiento: 0,
  //     Electricidad: 0,
  //     Estructura: 0,
  //     Muro: 0,
  //     Revestimiento: 0,
  //     Separacion: 0,
  //     Cod: "a1",
  //     Manzana_id: ""
  //   }
  //   this.viviendasService.add(vivienda);
  // }
}
