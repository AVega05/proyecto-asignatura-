import { Component, Input } from '@angular/core';
import { SolicitudService, Solicitud } from 'src/app/servicios/solicitud.service';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';

import * as moment from 'moment';
import { AlertController } from '@ionic/angular';
import { Vivienda, ViviendasService } from 'src/app/servicios/viviendas.service';

@Component({
    selector: 'detalleSolicitud',
    templateUrl: './detalleSolicitud.page.html',
    styleUrls: ['./detalleSolicitud.page.scss'],
})
export class DetalleSolicitud {

    @Input() solicitud: Solicitud
    @Input() vivienda: Vivienda
    date: string

    constructor(private modalCtrl: ModalController, private solicitudService: SolicitudService, private viviendasService: ViviendasService, private alertController: AlertController, private router: Router) {
        if (localStorage.getItem("user") == null) this.router.navigateByUrl("/login");
    }

    ngOnInit() {
        var date = new Date(this.solicitud.Fecha);
        this.date = moment(date).format('MM/DD/YYYY HH:mm');
    }

    async confirmarRecibido() {
        switch (this.solicitud.Kit) {
            case 'Estructura':
                this.vivienda.Estructura = 3;
                break;
            case 'Cimiento':
                this.vivienda.Cimiento = 3;
                break;
            case 'Revestimiento':
                this.vivienda.Revestimiento = 3;
                break;
            case 'Electricidad':
                this.vivienda.Electricidad = 3;
                break;
            case 'Separacion':
                this.vivienda.Separacion = 3;
                break;
            case 'Muro':
                this.vivienda.Muro = 3;
                break;
        }

        this.solicitud.Estado = 3;
        this.solicitud.Fecha = moment (new Date().toString()).format('MM/DD/YYYY HH:mm');

        this.viviendasService.update(this.vivienda);
        this.solicitudService.update(this.solicitud);

        this.closeModal();
    }

    closeModal() {
        this.modalCtrl.dismiss();
    }
}